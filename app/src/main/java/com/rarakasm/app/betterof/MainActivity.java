package com.rarakasm.app.betterof;

import android.animation.ValueAnimator;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements TodayFragment.OnFragmentInteractionListener,
        CommFragment.OnFragmentInteractionListener,
        ProductListFragment.OnFragmentInteractionListener,
        DashboardFragment.OnFragmentInteractionListener,
        ProductDetailFragment.OnFragmentInteractionListener,
        CommProductFragment.OnFragmentInteractionListener,
        CommArticleFragment.OnFragmentInteractionListener{

    private ViewPager mViewPager;
    private ContentPagerAdapter mAdapter;
    private TabLayout mTabLayout;
    private ImageView mHeaderBackground;
    private FrameLayout mPopup;
    private Animation mPopupRight, mPopupLeft;
    private int[] headerSizes = new int[]{56, 104, 56, 56};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPopup = (FrameLayout) findViewById(R.id.PopupContainer);
        mPopupRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);
        mPopupLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);

        mViewPager = (ViewPager) findViewById(R.id.ContentPager);
        mAdapter = new ContentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setOffscreenPageLimit(mAdapter.getCount() - 1);

        mTabLayout = (TabLayout) findViewById(R.id.BottomNav);



        mTabLayout.getTabAt(0).getIcon().setTint(getResources().getColor(R.color.colorPrimary));
        mTabLayout.getTabAt(1).getIcon().setTint(getResources().getColor(R.color.tabUnselected));
        mTabLayout.getTabAt(2).getIcon().setTint(getResources().getColor(R.color.tabUnselected));
        mTabLayout.getTabAt(3).getIcon().setTint(getResources().getColor(R.color.tabUnselected));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                tab.getIcon().setTint(getResources().getColor(R.color.colorPrimary));

                mViewPager.setCurrentItem(tab.getPosition());
                resizeHeader(dpToPx(headerSizes[tab.getPosition()]));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setTint(getResources().getColor(R.color.tabUnselected));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPager.addOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        mTabLayout.setScrollPosition(position, 0f, true);
                        mTabLayout.getTabAt(position).select();

                        if (position == 1){
                            CommFragment comm = (CommFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:"
                                    + R.id.ContentPager + ":" + position);
                            comm.expandAppbar();
                        }
                    }
                });

        mHeaderBackground = (ImageView) findViewById(R.id.HeaderBackground);

    }

    public void togglePopup(boolean state){
        if(state){
            mPopup.setVisibility(View.VISIBLE);
            mPopup.setOutlineProvider(ViewOutlineProvider.BOUNDS);
            mPopup.startAnimation(mPopupLeft);
        }else{
            mPopup.startAnimation(mPopupRight);
        }
    }

    public void resizeHeader(int headerSize) {

        ViewGroup.LayoutParams layout = mHeaderBackground.getLayoutParams();
        int beginValue = layout.height;
        int endValue = headerSize;

        ValueAnimator animator = ValueAnimator.ofInt(beginValue, endValue);
        animator.setDuration(128);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams =
                        (ViewGroup.LayoutParams) mHeaderBackground.getLayoutParams();
                layoutParams.height = (Integer) animation.getAnimatedValue();
                mHeaderBackground.setLayoutParams(layoutParams);
            }
        });
        animator.start();
    }

    public void resizeHeaderInstantly(int headerSize) {

        ViewGroup.LayoutParams layout = mHeaderBackground.getLayoutParams();
        layout.height = headerSize;
        mHeaderBackground.setLayoutParams(layout);
    }

    public int dpToPx(int dp){
        //DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        //return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));

        return Math.round(dp * getResources().getDisplayMetrics().density);
    }

    public int convertDpToPx(int dp){
        return Math.round(dp * getResources().getDisplayMetrics().density);
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            mPopupRight.reset();
            mPopupRight.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mPopup.setVisibility(View.GONE);
                    mPopup.setOutlineProvider(null);
                    getSupportFragmentManager().popBackStack();

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            togglePopup(false);
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

class ContentPagerAdapter extends FragmentPagerAdapter {

    public ContentPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return TodayFragment.newInstance();
            case 1: return CommFragment.newInstance("","");
            case 2: return ProductListFragment.newInstance("","");
            case 3: return DashboardFragment.newInstance("","");
        }
        return TodayFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 4;
    }
}
