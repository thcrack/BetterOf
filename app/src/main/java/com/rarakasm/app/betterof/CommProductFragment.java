package com.rarakasm.app.betterof;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CommFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CommFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CommProductFragment extends Fragment{

    static String RECYCLER_RESTORE_KEY = "recycler_restore";

    private OnFragmentInteractionListener mListener;
    private AppBarLayout mAppbarLayout;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecycler;
    private ProductListItemAdapter mAdapter;

    private DatabaseReference mDatabase;

    public CommProductFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CommProductFragment newInstance() {
        CommProductFragment fragment = new CommProductFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = FirebaseDatabase.getInstance().getReference("products");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comm_product, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mAppbarLayout = view.findViewById(R.id.Appbar);

        mRecycler = view.findViewById(R.id.ProductList);
        mRecycler.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        mRecycler.setNestedScrollingEnabled(false);
        mRecycler.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mAdapter = new ProductListItemAdapter(
                new FirebaseRecyclerOptions.Builder<Product>()
                        .setQuery(mDatabase.orderByChild("ratingScore"), Product.class)
                        .setLifecycleOwner(this)
                        .build());
        mAdapter.setOnItemClickListener(new ProductListItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i) {
                MainActivity activity = (MainActivity) getActivity();
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                activity.togglePopup(true);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ProductDetailFragment detailFragment = ProductDetailFragment.newInstance(mAdapter.getItem(i));
                fragmentTransaction.add(R.id.PopupContainer, detailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    public void expandAppbar(){
        mAppbarLayout.setExpanded(true, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle){
        bundle.putParcelable(RECYCLER_RESTORE_KEY, mRecycler.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mRecycler.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(RECYCLER_RESTORE_KEY));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
