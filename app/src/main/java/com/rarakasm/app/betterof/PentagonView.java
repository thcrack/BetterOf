package com.rarakasm.app.betterof;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * TODO: document your custom view class.
 */
public class PentagonView extends View {

    private Paint mPaintBack, mPaintFrontBorder, mPaintFrontFill, mPaintNodeFill;
    private TextPaint mTextPaintLabel, mTextPaintValue;
    private float mLabelTextWidth, mValueTextHeight;
    private Path mPathBack, mPathBorder, mPathNode;

    float mAnimateMultiplier = 1;

    private ArrayList<Float> mValues;
    private String[] mValueLabels;
    float[] nodePositionsX, nodePositionsY;

    public PentagonView(Context context) {
        super(context);
        init(null, 0);
    }

    public PentagonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public PentagonView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.PentagonView, defStyle, 0);

        a.recycle();

        mPaintBack = new Paint();
        mPaintBack.setColor(getResources().getColor(R.color.pentagon_back));
        mPaintBack.setStyle(Paint.Style.STROKE);
        mPaintBack.setStrokeWidth(2f);
        mPaintBack.setAntiAlias(true);
        mPaintBack.setDither(true);

        mPaintFrontBorder = new Paint();
        mPaintFrontBorder.setColor(getResources().getColor(R.color.pentagon_border));
        mPaintFrontBorder.setStyle(Paint.Style.STROKE);
        mPaintFrontBorder.setStrokeWidth(2f);
        mPaintFrontBorder.setAntiAlias(true);
        mPaintFrontBorder.setDither(true);

        mPaintFrontFill = new Paint();
        mPaintFrontFill.setColor(getResources().getColor(R.color.pentagon_fill));
        mPaintFrontFill.setStyle(Paint.Style.FILL);

        mPaintNodeFill = new Paint();
        mPaintNodeFill.setColor(Color.WHITE);
        mPaintNodeFill.setStyle(Paint.Style.FILL);

        mPathBack = new Path();
        mPathBorder = new Path();
        mPathNode = new Path();

        mValueLabels = getResources().getStringArray(R.array.pentagon_labels);

        mTextPaintLabel = new TextPaint();

        mTextPaintLabel.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaintLabel.setTextAlign(Paint.Align.CENTER);
        mTextPaintLabel.setTextSize(14f * getResources().getDisplayMetrics().scaledDensity);
        mTextPaintLabel.setColor(Color.GRAY);
        mLabelTextWidth = mTextPaintLabel.measureText(mValueLabels[0]) * 0.75f;

        mTextPaintValue = new TextPaint();

        mTextPaintValue.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaintValue.setTextAlign(Paint.Align.CENTER);
        mTextPaintValue.setTextSize(10f * getResources().getDisplayMetrics().scaledDensity);
        mTextPaintValue.setColor(getResources().getColor(R.color.pentagon_border));
        mValueTextHeight = mTextPaintValue.getFontMetrics().top * 1.5f;

        nodePositionsX = new float[5];
        nodePositionsY = new float[5];
    }

    public void setValues(ArrayList<Float> values){
        if(values.size() != 5){
            throw new RuntimeException("Setting invalid values to PentagonView");
        }
        mValues = values;
    }

    public ArrayList<Float> getValues(){
        return mValues;
    }

    public void animateMultiplier(){
        ValueAnimator v = ValueAnimator.ofFloat(0, 1f);
        v.setDuration(750);
        v.setInterpolator(new DecelerateInterpolator());
        v.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAnimateMultiplier = (float)animation.getAnimatedValue();
                invalidate();
            }
        });
        v.start();
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        double pivotX = paddingLeft + contentWidth / 2;
        double pivotY = paddingTop + contentHeight / 2;
        double radius = contentHeight / 2;
        double radiusStep = radius / 5;

        mPathBack.reset();
        mPathBorder.reset();
        mPathNode.reset();

        ArrayList<Float> values = getValues();

        if(values != null){
            mPathBorder.moveTo((float)pivotX, (float)(pivotY - radius * Math.min(mAnimateMultiplier, (values.get(0) / 5))));
            for(int r = 0; r < 5; r++){
                double startAngle = -Math.PI / 2;
                mPathBack.moveTo((float)pivotX, (float)(pivotY - radius));
                for(int i = 1; i <= 5; i++){
                    float pX = (float)(radius * Math.cos(startAngle + Math.PI * i * 2 / 5) + pivotX);
                    float pY = (float)(radius * Math.sin(startAngle + Math.PI * i * 2 / 5) + pivotY);
                    mPathBack.lineTo(pX, pY);
                    if(r == 0){
                        mPathBack.moveTo((float)pivotX, (float)pivotY);
                        mPathBack.lineTo(pX, pY);
                        nodePositionsX[i - 1] = pX;
                        nodePositionsY[i - 1] = pY;

                        float opX = (float)(radius * Math.min(mAnimateMultiplier, (values.get(i % 5) / 5)) * Math.cos(startAngle + Math.PI * i * 2 / 5) + pivotX);
                        float opY = (float)(radius * Math.min(mAnimateMultiplier, (values.get(i % 5) / 5)) * Math.sin(startAngle + Math.PI * i * 2 / 5) + pivotY);

                        mPathBorder.lineTo(opX, opY);
                        mPathNode.addCircle(opX, opY, 6, Path.Direction.CW);
                    }
                }
                radius -= radiusStep;
            }

            mPathBack.close();
            canvas.drawPath(mPathBack, mPaintBack);

            mPathBorder.close();
            canvas.drawPath(mPathBorder, mPaintFrontFill);
            canvas.drawPath(mPathBorder, mPaintFrontBorder);

            mPathNode.close();
            canvas.drawPath(mPathNode, mPaintNodeFill);
            canvas.drawPath(mPathNode, mPaintFrontBorder);

            float density = getResources().getDisplayMetrics().scaledDensity;
            for(int i = 0; i < 5; i++){
                float offsetX = 0, offsetY = 0;
                switch (i){
                    case 0: offsetX = mLabelTextWidth; break;
                    case 1: offsetX = mLabelTextWidth; offsetY = -mValueTextHeight; break;
                    case 2: offsetX = -mLabelTextWidth; offsetY = -mValueTextHeight; break;
                    case 3: offsetX = -mLabelTextWidth; break;
                    case 4: offsetY = mValueTextHeight * 1.25f; break;
                }
                canvas.drawText(mValueLabels[i], nodePositionsX[i] + offsetX, nodePositionsY[i] + offsetY, mTextPaintLabel);
                canvas.drawText(values.get((i + 1) % 5).toString(), nodePositionsX[i] + offsetX, nodePositionsY[i] + offsetY - mValueTextHeight, mTextPaintValue);
            }
        }

        super.onDraw(canvas);
    }
}
