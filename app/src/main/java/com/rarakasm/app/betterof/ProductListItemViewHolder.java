package com.rarakasm.app.betterof;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class ProductListItemViewHolder extends RecyclerView.ViewHolder{
    Product product;
    View itemView;
    TextView indexText;
    ImageView thumbnailImage;
    TextView nameText;
    TextView tagsText;
    RatingBar ratingBar;
    TextView ratingCountText;
    TextView priceText;

    public void bindViews(TextView indexText, ImageView thumbnailImage, TextView nameText, TextView tagsText, RatingBar ratingBar, TextView ratingCountText, TextView priceText){
        this.indexText = indexText;
        this.thumbnailImage = thumbnailImage;
        this.nameText = nameText;
        this.tagsText = tagsText;
        this.ratingBar = ratingBar;
        this.ratingCountText = ratingCountText;
        this.priceText = priceText;
    }

    public ProductListItemViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }
}
