package com.rarakasm.app.betterof;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateProductDataActivity extends AppCompatActivity implements View.OnClickListener{

    private final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;
    private static final String TAG = "DATA_MESSAGE";
    // Firebase instance variables
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mFirebaseStorageReference;

    private EditText inputName, inputTags, inputPrice, inputRatingScore, inputRatingCount, inputLikeCount,
                     inputProductDescription, inputProductIngredient, inputProductCaution, inputPentagonRatings;

    private ImageView mImageView;
    private Drawable imagePlaceholder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product_data);

        mImageView = (ImageView) findViewById(R.id.ImagePreview);
        imagePlaceholder = mImageView.getDrawable();

        inputName = (EditText) findViewById(R.id.InputName);
        inputTags = (EditText) findViewById(R.id.InputTags);
        inputPrice = (EditText) findViewById(R.id.InputPrice);
        inputRatingScore = (EditText) findViewById(R.id.InputRatingScore);
        inputRatingCount = (EditText) findViewById(R.id.InputRatingCount);
        inputLikeCount = (EditText) findViewById(R.id.InputLikeCount);
        inputProductDescription = (EditText) findViewById(R.id.InputProductDescription);
        inputProductIngredient = (EditText) findViewById(R.id.InputProductIngredient);
        inputProductCaution = (EditText) findViewById(R.id.InputProductCaution);
        inputPentagonRatings = (EditText) findViewById(R.id.InputPentagonRatings);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference("");
        mFirebaseStorage = FirebaseStorage.getInstance();
        mFirebaseStorageReference = mFirebaseStorage.getReference("product_images");
//        mFirebaseDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // This method is called once with the initial value and again
//                // whenever data at this location is updated.
//                String value = dataSnapshot.getValue(String.class);
//                Log.d(TAG, "Value is: " + value);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        switch (i){
            case R.id.edit_item_send_button:
                Product newProduct = tryParseInputToProduct();
                if(newProduct != null){
                    writeNewProduct(newProduct);
                }
                break;

            case R.id.ChoosePhotoButton:
                choosePhoto();
                break;
        }
    }

    private Product tryParseInputToProduct() {
        if(inputName.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Name is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputTags.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Tags are empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputPrice.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Price is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputRatingScore.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Rating Score is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputRatingCount.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Rating Count is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputLikeCount.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Like Count is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputProductDescription.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Product Description is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }
        if(inputProductIngredient.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Product Ingredient is empty", Snackbar.LENGTH_LONG).show();
            return null;
        }

        ArrayList<Float> ratingsFloat = new ArrayList<>();

        if(inputPentagonRatings.getText().toString().isEmpty()){
            Snackbar.make(findViewById(R.id.coordinator), "Pentagon Ratings are empty", Snackbar.LENGTH_LONG).show();
            return null;
        }else{
            String[] ratings = inputPentagonRatings.getText().toString().split(",");
            if(ratings.length != 5){
                Snackbar.make(findViewById(R.id.coordinator), "Pentagon Ratings must contain 5 values", Snackbar.LENGTH_LONG).show();
                return null;
            }

            try {
                for(int i = 0; i < ratings.length; i++){
                    ratingsFloat.add(Float.parseFloat(ratings[i]));
                }
            }catch (NumberFormatException e){
                Snackbar.make(findViewById(R.id.coordinator), "One or more pentagon rating values are invalid", Snackbar.LENGTH_LONG).show();
                return null;
            }
        }

        if(filePath == null){
            Snackbar.make(findViewById(R.id.coordinator), "No image is selected", Snackbar.LENGTH_LONG).show();
            return null;
        }

        return new Product(
                            inputName.getText().toString(),
                            inputTags.getText().toString(),
                            Integer.parseInt(inputPrice.getText().toString()),
                            Float.parseFloat(inputRatingScore.getText().toString()),
                            Integer.parseInt(inputRatingCount.getText().toString()),
                            Integer.parseInt(inputLikeCount.getText().toString()),
                            inputProductDescription.getText().toString(),
                            inputProductIngredient.getText().toString(),
                            inputProductCaution.getText().toString(),
                            ratingsFloat
        );
    }

    private void clearInputFields(){
        inputName.setText("");
        inputTags.setText("");
        inputPrice.setText("");
        inputRatingScore.setText("");
        inputRatingCount.setText("");
        inputLikeCount.setText("");
        inputProductDescription.setText("");
        inputProductIngredient.setText("");
        inputProductCaution.setText("");
        inputPentagonRatings.setText("");
        mImageView.setImageDrawable(imagePlaceholder);
    }

    private void choosePhoto(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null )
        {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ((ImageView) findViewById(R.id.ImagePreview)).setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void writeNewProduct(Product product) {


        final DatabaseReference newItem = mFirebaseDatabaseReference.child("products").push();
        product.setId(newItem.getKey());
        final Product newProduct = product;

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        StorageReference ref = mFirebaseStorageReference.child(newItem.getKey());
        ref.putFile(filePath)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        newItem.setValue(newProduct).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialog.dismiss();
                                Toast.makeText(CreateProductDataActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                                Snackbar.make(findViewById(R.id.coordinator), "Product data sent!", Snackbar.LENGTH_LONG).show();
                                clearInputFields();
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        newItem.removeValue();
                        Toast.makeText(CreateProductDataActivity.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                .getTotalByteCount());
                        progressDialog.setMessage("Uploaded "+(int)progress+"%");
                    }
                });
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }
}
