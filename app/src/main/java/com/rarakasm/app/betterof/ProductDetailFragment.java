package com.rarakasm.app.betterof;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters

    private OnFragmentInteractionListener mListener;
    private ActionBar mSourceActionBar;
    private Product mProduct;
    private DatabaseReference mProductReference, mUserReference;
    private String uid;

    private ImageView mProductImage;
    private TextView mProductName;
    private TextView mProductTags;
    private TextView mProductPrice;
    private RatingBar mProductRatingScore;
    private TextView mProductRatingCount;

    private ImageButton mLikeButton;
    private TextView mLikeCount;
    private int mLikeCountBuffer;
    private boolean hasLiked = false;

    private ImageButton mBookmarkButton;
    private boolean hasBookmarked = false;

    private Animation mFlipAnimation, mFlipBackAnimation, mFadeIn, mFadeOut, mLikeButtonClick, mBookmarkButtonClick;
    private TextView mFeatureText;
    private View mIngredient;
    private ImageView mIngredientExpandIcon;
    private ExpandableLayout mIngredientExpandable;
    private TextView mIngredientText;
    private View mPentagon;
    private ImageView mPentagonExpandIcon;
    private ExpandableLayout mPentagonExpandable;
    private PentagonView mPentagonView;

    public ProductDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProductDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductDetailFragment newInstance(Product product) {
        ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("PRODUCT", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mProduct = getArguments().getParcelable("PRODUCT");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_product_detail, container, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.TopBar);
        setHasOptionsMenu(true);
        TypedArray a = getActivity().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[] {android.R.attr.homeAsUpIndicator});
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable drawable = getResources().getDrawable(attributeResourceId);
        a.recycle();
        toolbar.setNavigationIcon(drawable);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mProductReference = FirebaseDatabase.getInstance().getReference("products/" + mProduct.getId());
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mUserReference = FirebaseDatabase.getInstance().getReference("users/" + uid);

        mProductReference.child("liked_users/" + uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean result = (dataSnapshot.getValue() == null) ? false : (boolean)dataSnapshot.getValue();
                initLikeState(result);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        mProductReference.child("bookmarked_users/" + uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean result = (dataSnapshot.getValue() == null) ? false : (boolean)dataSnapshot.getValue();
                initBookmarkState(result);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        toolbar.setTitle(mProduct.getName());

        mProductImage = v.findViewById(R.id.ProductImage);
        StorageReference storageRef = FirebaseStorage.getInstance().getReference("product_images/" + mProduct.getId());
        GlideApp.with(v)
                .load(storageRef)
                .into(mProductImage);

        mProductName = v.findViewById(R.id.ProductName);
        mProductTags = v.findViewById(R.id.ProductTags);
        mProductPrice = v.findViewById(R.id.ProductPrice);
        mProductRatingScore = v.findViewById(R.id.ProductRatingScore);
        mProductRatingCount = v.findViewById(R.id.ProductRatingCount);

        mProductName.setText(mProduct.getName());
        mProductTags.setText(mProduct.getFormattedTags());
        mProductPrice.setText("$" + mProduct.getPrice());
        mProductRatingScore.setRating(Math.min(mProduct.getRatingScore(), 5));
        mProductRatingCount.setText("(" + mProduct.getRatingCount() + ")");

        Context c = getActivity().getApplicationContext();
        mFlipAnimation = AnimationUtils.loadAnimation(c, R.anim.flip);
        mFlipBackAnimation = AnimationUtils.loadAnimation(c, R.anim.flip_back);
        mFadeIn = AnimationUtils.loadAnimation(c, R.anim.fade_in);
        mFadeOut = AnimationUtils.loadAnimation(c, R.anim.fade_out);
        mLikeButtonClick = AnimationUtils.loadAnimation(c, R.anim.button_click);
        mBookmarkButtonClick = AnimationUtils.loadAnimation(c, R.anim.button_click);

        mLikeButton = v.findViewById(R.id.Like);
        mLikeCount = v.findViewById(R.id.LikeCount);
        mLikeCountBuffer = mProduct.getLikeCount();
        mLikeCount.setText(mLikeCountBuffer + "");

        mLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasLiked){
                    int c = getResources().getColor(R.color.button_selected);
                    mLikeButton.setColorFilter(c);
                    mLikeCount.setTextColor(c);
                    mLikeButton.startAnimation(mLikeButtonClick);
                    mLikeCountBuffer++;
                    mLikeCount.setText(mLikeCountBuffer + "");
                    mProductReference.child("liked_users/" + uid).setValue(true);
                    mProductReference.child("likeCount").runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {
                            mutableData.setValue((long)mutableData.getValue() + 1);
                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                            //Toast.makeText(getActivity(), "Completed: " + databaseError.getMessage(), Toast.LENGTH_LONG).show();
                            mLikeCountBuffer = (int)(long)dataSnapshot.getValue();
                            mLikeCount.setText(mLikeCountBuffer + "");
                        }
                    });
                    mUserReference.child("liked_products/" + mProduct.getId()).setValue(true);
                }else{
                    int c = getResources().getColor(R.color.button_unselected);
                    mLikeButton.setColorFilter(c);
                    mLikeCount.setTextColor(c);
                    mLikeCountBuffer--;
                    mLikeCount.setText(mLikeCountBuffer + "");
                    mProductReference.child("liked_users/" + uid).setValue(false);
                    mUserReference.child("liked_products/" + mProduct.getId()).setValue(false);
                    mProductReference.child("likeCount").runTransaction(new Transaction.Handler() {
                        @Override
                        public Transaction.Result doTransaction(MutableData mutableData) {
                            mutableData.setValue((long)mutableData.getValue() - 1);
                            return Transaction.success(mutableData);
                        }

                        @Override
                        public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                            mLikeCountBuffer = (int)(long)dataSnapshot.getValue();
                            mLikeCount.setText(mLikeCountBuffer + "");
                        }
                    });
                }
                hasLiked = !hasLiked;
            }
        });

        mBookmarkButton = v.findViewById(R.id.Bookmark);

        mBookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasBookmarked){
                    int c = getResources().getColor(R.color.button_selected);
                    mBookmarkButton.setColorFilter(c);
                    mBookmarkButton.startAnimation(mBookmarkButtonClick);
                    mProductReference.child("bookmarked_users/" + uid).setValue(true);
                    mUserReference.child("bookmarked_products/" + mProduct.getId()).setValue(true);
                }else{
                    int c = getResources().getColor(R.color.button_unselected);
                    mBookmarkButton.setColorFilter(c);
                    mProductReference.child("bookmarked_users/" + uid).setValue(false);
                    mUserReference.child("bookmarked_products/" + mProduct.getId()).setValue(false);
                }
                hasBookmarked = !hasBookmarked;
            }
        });

        mFeatureText = v.findViewById(R.id.FeatureText);
        mFeatureText.setText(mProduct.getProductDescription());

        mIngredient = v.findViewById(R.id.IngredientLabelView);
        mIngredientExpandIcon = mIngredient.findViewById(R.id.ExpandIcon);
        mIngredientExpandable = v.findViewById(R.id.ExpandableIngredient);
        mIngredientText = mIngredientExpandable.findViewById(R.id.ExpandableContent);
        mIngredientText.setText(mProduct.getProductIngredient());

        mIngredient.findViewById(R.id.ClickBox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIngredientExpandable.isExpanded()){
                    mIngredientExpandIcon.startAnimation(mFlipAnimation);
                    mIngredientText.startAnimation(mFadeIn);
                }else{
                    mIngredientExpandIcon.startAnimation(mFlipBackAnimation);
                    mIngredientText.startAnimation(mFadeOut);
                }
                mIngredientExpandable.toggle();
            }
        });

        mPentagon = v.findViewById(R.id.PentagonLabelView);
        mPentagonExpandIcon = mPentagon.findViewById(R.id.ExpandIcon);
        mPentagonExpandable = v.findViewById(R.id.ExpandablePentagon);
        mPentagonView = mPentagonExpandable.findViewById(R.id.Pentagon);
        mPentagonView.setValues(mProduct.getPentagonRatings());

        mPentagon.findViewById(R.id.ClickBox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPentagonExpandable.isExpanded()){
                    mPentagonExpandIcon.startAnimation(mFlipAnimation);
                    mPentagonView.startAnimation(mFadeIn);
                    mPentagonView.animateMultiplier();
                }else{
                    mPentagonExpandIcon.startAnimation(mFlipBackAnimation);
                    mPentagonView.startAnimation(mFadeOut);
                }
                mPentagonExpandable.toggle();
            }
        });

        return v;
    }

    void initLikeState(boolean state){
        hasLiked = state;
        if(hasLiked){
            int c = getResources().getColor(R.color.button_selected);
            mLikeButton.setColorFilter(c);
            mLikeCount.setTextColor(c);
        }else{
            int c = getResources().getColor(R.color.button_unselected);
            mLikeButton.setColorFilter(c);
            mLikeCount.setTextColor(c);
        }
    }

    void initBookmarkState(boolean state){
        hasBookmarked = state;
        if(hasBookmarked){
            int c = getResources().getColor(R.color.button_selected);
            mBookmarkButton.setColorFilter(c);
        }else{
            int c = getResources().getColor(R.color.button_unselected);
            mBookmarkButton.setColorFilter(c);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
