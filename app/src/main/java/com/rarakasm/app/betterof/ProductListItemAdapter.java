package com.rarakasm.app.betterof;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ProductListItemAdapter extends FirebaseRecyclerAdapter<Product, ProductListItemViewHolder>
                                    implements View.OnClickListener{

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ProductListItemAdapter(@NonNull FirebaseRecyclerOptions<Product> options) {
        super(options);
    }

    private OnItemClickListener onItemClickListener = null;

    @Override
    public void onClick(View v) {
        if(onItemClickListener != null){
            onItemClickListener.onItemClick(v, (int)v.getTag());
        }
    }

    public static interface OnItemClickListener{
        void onItemClick(View view, int i);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    @Override
    public ProductListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_product_list_item, parent, false);
        ProductListItemViewHolder vh = new ProductListItemViewHolder(v);
        vh.bindViews(
                (TextView) v.findViewById(R.id.Index),
                (ImageView) v.findViewById(R.id.Image),
                (TextView) v.findViewById(R.id.ProductName),
                (TextView) v.findViewById(R.id.ProductTags),
                (RatingBar) v.findViewById(R.id.RatingScore),
                (TextView) v.findViewById(R.id.RatingCount),
                (TextView) v.findViewById(R.id.Price)
        );
        v.setOnClickListener(this);
        return vh;
    }

    @Override
    protected void onBindViewHolder(@NonNull ProductListItemViewHolder holder, int i, @NonNull Product product) {

        holder.product = product;
        holder.indexText.setText(String.format("%02d", i + 1));
        holder.nameText.setText(product.getName());
        holder.tagsText.setText(product.getFormattedTags());
        holder.ratingBar.setRating(Math.min(product.getRatingScore(), 5));
        holder.ratingCountText.setText("(" + product.getRatingCount() + ")");
        holder.priceText.setText("$" + product.getPrice());

        StorageReference storageRef = FirebaseStorage.getInstance().getReference("product_images/" + product.getId());
        GlideApp.with(holder.itemView)
                .load(storageRef)
                .into(holder.thumbnailImage);

        holder.itemView.setTag(i);
    }

    @NonNull
    @Override
    public Product getItem(int position) {
        return super.getItem(getItemCount() - 1 - position);
    }
}
