package com.rarakasm.app.betterof;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

class Product implements Parcelable {

    private String id;
    private String name;
    private String tags;
    private int price;
    private float ratingScore;
    private int ratingCount;
    private int likeCount;

    private String formattedTags = "";

    private String productDescription;
    private String productIngredient;
    private String productCaution;
    private ArrayList<Float> pentagonRatings;

    public Product() {
    }

    public Product(String name, String tags, int price, float ratingScore, int ratingCount, int likeCount,
                   String productDescription, String productIngredient, String productCaution, ArrayList<Float> pentagonRatings) {
        this.name = name;
        this.tags = tags;
        this.price = price;
        this.ratingScore = ratingScore;
        this.ratingCount = ratingCount;
        this.likeCount = likeCount;
        this.productDescription = productDescription;
        this.productIngredient = productIngredient;
        this.productCaution = productCaution;
        this.pentagonRatings = pentagonRatings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public String getFormattedTags(){

        if(!formattedTags.equals("")) return formattedTags;
        String[] tagArray = getTags().split(",");
        StringBuilder b = new StringBuilder();
        for(int i = 0; i < tagArray.length; i++){
            tagArray[i] = tagArray[i].trim();
            tagArray[i] = "#" + tagArray[i] + " ";
            b.append(tagArray[i]);
        }
        formattedTags = b.toString();
        return formattedTags;
    }

    public int getPrice() {
        return price;
    }

    public float getRatingScore() {
        return ratingScore;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public String getProductIngredient() {
        return productIngredient;
    }

    public String getProductCaution() {
        return productCaution;
    }

    public ArrayList<Float> getPentagonRatings() {
        return pentagonRatings;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.tags);
        dest.writeString(this.formattedTags);
        dest.writeInt(this.price);
        dest.writeFloat(this.ratingScore);
        dest.writeInt(this.ratingCount);
        dest.writeInt(this.likeCount);
        dest.writeString(this.productDescription);
        dest.writeString(this.productIngredient);
        dest.writeString(this.productCaution);
        dest.writeList(this.pentagonRatings);
    }

    protected Product(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.tags = in.readString();
        this.formattedTags = in.readString();
        this.price = in.readInt();
        this.ratingScore = in.readFloat();
        this.ratingCount = in.readInt();
        this.likeCount = in.readInt();
        this.productDescription = in.readString();
        this.productIngredient = in.readString();
        this.productCaution = in.readString();
        this.pentagonRatings = new ArrayList<Float>();
        in.readList(this.pentagonRatings, Float.class.getClassLoader());
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
